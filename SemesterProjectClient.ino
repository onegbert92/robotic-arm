/**************************
  Client Code
  Motors for Reach, OMG, Yaw, Clamp
  Olive and Tucker Final Project 
**************************/

#include "BLEDevice.h"
#include <ESP32Servo.h>
#include <Wire.h>

///////////////////////////////* MOTOR VARIABLES*//////////////////////////////////////////

Servo servoReach;
Servo servoOmg;
Servo servoYaw;
Servo servoClamp;

// Servo initialization
int reachPos = 50;
int omgPos = 50;
int yawPos = 90;
int clampPos = 15;

// Servo signal pins
int REACHPIN = 19;
int OMGPIN = 18;
int YAWPIN = 5;
int CLAMPPIN = 21;

// Servo position limits
double reachMAX = 100; // Passenger right
double omgMAX = 100; // driver left
double yawMAX = 180;
double clampMAX = 100;
double MIN = 0;

//EMG trigger value
const int THRESH = 3000;

// Timer variables
unsigned long lastTime = 0;
unsigned long timerDelay = 50; 

// State Variables
bool nOmg = 0;
bool fOmg = 0;
bool bOmg = 0;

bool nRea = 0;
bool fRea = 0;
bool bRea = 0;

bool nYaw = 0;
bool fYaw = 0;
bool bYaw = 0;

bool nCla = 0;
bool Cla = 0; //(1) for Open, (0) for close 

//Variables to store accelerometer values
char* reachChar;
char* omgChar;
char* yawChar;
char* clampChar;

//Flags to check whether new accelerometer/EMG readings are available
boolean newR = false;
boolean newO = false;
boolean newY = false;
boolean newC = false;

// Convert string from the server back into a double
float reachVal;
float omgVal;
float yawVal;
float clampVal;

String rtemp;
String otemp;
String ytemp;
String ctemp;


/////////////////////////////* BLUETOOTH FUNCTIONS*///////////////////////////////////////////////

//BLE Server name (the other ESP32 name running the server sketch)
#define bleServerName "OT_ESP32"

/* UUID's of the service, characteristic that we want to read*/
// BLE Service
static BLEUUID botServiceUUID("eb8f7231-c8f4-4b54-a38e-99507c501c0e");

// Reach-Axis
static BLEUUID reachCharacteristicUUID("4bc66632-6c6a-41d5-adf6-5dc4e2b04c80");

// OMG-Axis
static BLEUUID omgCharacteristicUUID("92d4c053-f0e6-4d73-ad7a-76c486de19ba");

// Yaw-Axis
static BLEUUID yawCharacteristicUUID("2f4d7732-2020-4e93-8d56-ea90122ecff0");

// Clamp-Axis
static BLEUUID clampCharacteristicUUID("f3c9d0f3-dbcb-4219-870c-e776fd34d371");


//Flags stating if should begin connecting and if the connection is up
static boolean doConnect = false;
static boolean connected = false;

//Address of the peripheral device. Address will be found during scanning...
static BLEAddress *pServerAddress;

//Characteristics that we want to read
static BLERemoteCharacteristic* reachCharacteristic;
static BLERemoteCharacteristic* omgCharacteristic;
static BLERemoteCharacteristic* yawCharacteristic;
static BLERemoteCharacteristic* clampCharacteristic;

//Activate notify
const uint8_t notificationOn[] = {0x1, 0x0};
const uint8_t notificationOff[] = {0x0, 0x0};

//Connect to the BLE Server that has the name, Service, and Characteristics
bool connectToServer(BLEAddress pAddress) {
  BLEClient* pClient = BLEDevice::createClient();

  // Connect to the remove BLE Server.
  pClient->connect(pAddress);
  Serial.println(" - Connected to server");

  // Obtain a reference to the service we are after in the remote BLE server.
  BLERemoteService* pRemoteService = pClient->getService(botServiceUUID);
  if (pRemoteService == nullptr) {
    Serial.print("Failed to find our service UUID: ");
    Serial.println(botServiceUUID.toString().c_str());
    return (false);
  }

  // Obtain a reference to the characteristics in the service of the remote BLE server.
  reachCharacteristic = pRemoteService->getCharacteristic(reachCharacteristicUUID);
  omgCharacteristic = pRemoteService->getCharacteristic(omgCharacteristicUUID);
  yawCharacteristic = pRemoteService->getCharacteristic(yawCharacteristicUUID);
  clampCharacteristic = pRemoteService->getCharacteristic(clampCharacteristicUUID);

  if (reachCharacteristic == nullptr || omgCharacteristic == nullptr || yawCharacteristic == nullptr || clampCharacteristic == nullptr) {
    Serial.print("Failed to find our characteristic UUID");
    return false;
  }
  Serial.println(" - Found our characteristics");

  //Assign callback functions for the Characteristics
  reachCharacteristic->registerForNotify(reachNotifyCallback);
  omgCharacteristic->registerForNotify(omgNotifyCallback);
  yawCharacteristic->registerForNotify(yawNotifyCallback);
  clampCharacteristic->registerForNotify(clampNotifyCallback);
  return true;
}

//Callback function that gets called, when another device's advertisement has been received
class MyAdvertisedDeviceCallbacks: public BLEAdvertisedDeviceCallbacks {
    void onResult(BLEAdvertisedDevice advertisedDevice) {
      if (advertisedDevice.getName() == bleServerName) { //Check if the name of the advertiser matches
        advertisedDevice.getScan()->stop(); //Scan can be stopped, we found what we are looking for
        pServerAddress = new BLEAddress(advertisedDevice.getAddress()); //Address of advertiser is the one we need
        doConnect = true; //Set indicator, stating that we are ready to connect
        Serial.println("Device found. Connecting!");
      }
    }
};

//When the BLE Server sends a new reach-axis reading with the notify property
static void reachNotifyCallback(BLERemoteCharacteristic* pBLERemoteCharacteristic,
                                uint8_t* pData, size_t length, bool isNotify) {
  //store reach-axis value
  reachChar = (char*)pData;
  newR = true;
}

//When the BLE Server sends a new omg-axis reading with the notify property
static void omgNotifyCallback(BLERemoteCharacteristic* pBLERemoteCharacteristic,
                              uint8_t* pData, size_t length, bool isNotify) {
  //store omg-axis value
  omgChar = (char*)pData;
  newO = true;
}

//When the BLE Server sends a new yaw-axis reading with the notify property
static void yawNotifyCallback(BLERemoteCharacteristic* pBLERemoteCharacteristic,
                              uint8_t* pData, size_t length, bool isNotify) {
  //store yaw value
  yawChar = (char*)pData;
  newY = true;
}

//When the BLE Server sends a new clamp-axis EMG reading with the notify property
static void clampNotifyCallback(BLERemoteCharacteristic* pBLERemoteCharacteristic,
                                uint8_t* pData, size_t length, bool isNotify) {
  //store clamp value
  clampChar = (char*)pData;
  newC = true;
}



void setup() {
  Serial.begin(9600);

  //Init BLE device
  BLEDevice::init("");

  // Retrieve a Scanner and set the callback we want to use to be informed when we
  // have detected a new device.  Specify that we want active scanning and start the
  // scan to run for 30 seconds.
  BLEScan* pBLEScan = BLEDevice::getScan();
  pBLEScan->setAdvertisedDeviceCallbacks(new MyAdvertisedDeviceCallbacks());
  pBLEScan->setActiveScan(true);
  pBLEScan->start(30);

  // Servo PWM timers
  ESP32PWM::allocateTimer(0);
  ESP32PWM::allocateTimer(1);
  ESP32PWM::allocateTimer(2);
  ESP32PWM::allocateTimer(3);
  servoReach.setPeriodHertz(50);    // 50 hz servo

  servoReach.attach(REACHPIN);
  servoOmg.attach(OMGPIN);
  servoYaw.attach(YAWPIN);
  servoClamp.attach(CLAMPPIN);

  servoReach.write(reachPos);
  servoOmg.write(omgPos);
  servoYaw.write(yawPos);
  servoClamp.write(clampPos);

}


void loop() {
  rtemp = reachChar;
  otemp = omgChar;
  ytemp = yawChar;
  ctemp = clampChar;

  // If the flag "doConnect" is true, BLE server has been found. Connect to server:
  if (doConnect == true) {
    if (connectToServer(*pServerAddress)) {
      //Activate the Notify property of each Characteristic
      reachCharacteristic->getDescriptor(BLEUUID((uint16_t)0x2902))->writeValue((uint8_t*)notificationOn, 2, true);
      omgCharacteristic->getDescriptor(BLEUUID((uint16_t)0x2902))->writeValue((uint8_t*)notificationOn, 2, true);
      yawCharacteristic->getDescriptor(BLEUUID((uint16_t)0x2902))->writeValue((uint8_t*)notificationOn, 2, true);
      clampCharacteristic->getDescriptor(BLEUUID((uint16_t)0x2902))->writeValue((uint8_t*)notificationOn, 2, true);
      connected = true;
      doConnect = false;
    }
  }


  reachVal = rtemp.toFloat();
  omgVal = otemp.toFloat();
  yawVal = ytemp.toFloat();
  clampVal = ctemp.toFloat();

  //if new readings are available, reset flags
  if (newR && newO && newY && newC) {
    newR = false;
    newO = false;
    newY = false;
    newC = false;
  }

  if ((millis() - lastTime) > timerDelay) {

    // Servo 1 - OMG Motion
    if (omgVal <= -0.30 && omgVal >= -0.50) { //Hold current position
      nOmg = true;
      bOmg = false;
      fOmg = false;
    }
    else if (omgVal < -0.50) { //Rotate right, forward
      nOmg = false;
      bOmg = false;
      fOmg = true;
    }
    else if (omgVal > -0.30) { //Rotate left, backward
      nOmg = false;
      bOmg = true;
      fOmg = false;
    }

    // Servo 2 - YAW
    if (yawVal <= -0.30 && yawVal >= -0.60) { //Hold current position
      nYaw = true;
      bYaw = false;
      fYaw = false;
    }
    else if (yawVal < -0.60) { //Rotate right
      nYaw = false;
      bYaw = false;
      fYaw = true;
    }
    else if (yawVal > -0.30) { //Rotate left
      nYaw = false;
      bYaw = true;
      fYaw = false;
    }

    // Servo 3 - REACH
    if (reachVal <= -0.85) { //Hold current position
      nRea = true;
      bRea = false;
      fRea = false;
    }
    else if (reachVal >= -0.25 && reachVal <= 0) { // Forward
      nRea = false;
      bRea = false;
      fRea = true;
    }
    else if (reachVal < -0.25 && reachVal > -0.85) { // Backward
      nRea = false;
      bRea = true;
      fRea = false;
    }

    // Servo 4 - CLAMP (EMG)
    if (clampVal <= THRESH) {
      nCla = false;
      Cla = !Cla;
    }
    else {
      nCla = true;
    }



    // Servo 1 - OMG Motion
    if (fOmg && (omgPos < (omgMAX - 2))) {
      //Serial.print("OMG Forward. Position: "); Serial.print(omgPos); Serial.print("OMG Val: "); Serial.println(omgVal);
      omgPos = omgPos + 2;
      servoOmg.write(omgPos);
    }

    else if (bOmg && (omgPos > (MIN + 2))) {
      //Serial.print("OMG Backward. Position: "); Serial.print(omgPos); Serial.print("OMG Val: "); Serial.println(omgVal);
      omgPos = omgPos - 2;
      servoOmg.write(omgPos);
    }

    else if (nOmg) {
      omgPos = omgPos;
      //Serial.print("OMG Neutral. Position: "); Serial.print(omgPos); Serial.print("omgVal: "); Serial.println(omgVal);
      servoOmg.write(omgPos);
    }

    // Servo 2 - YAW
    if (fYaw && (yawPos < (yawMAX - 2))) {
      //Serial.print("YAW Forward. Position: "); Serial.print(yawPos); Serial.print("yawVal: "); Serial.println(yawVal);
      yawPos = yawPos + 2;
      servoYaw.write(yawPos);
    }

    else if (bYaw && (yawPos > (MIN + 2))) {
      //Serial.print("YAW Backward. Position: "); Serial.print(yawPos); Serial.print("yawVal: "); Serial.println(yawVal);
      yawPos = yawPos - 2;
      servoYaw.write(yawPos);
    }

    else if (nYaw) {
      yawPos = yawPos;
      //Serial.print("YAW Neutral. Position: "); Serial.print(yawPos); Serial.print("yawVal: "); Serial.println(yawVal);
      servoYaw.write(yawPos);
    }

    // Servo 3 - REACH
    if (fRea && (reachPos < (reachMAX - 2))) {
      reachPos = reachPos + 2;
      servoReach.write(reachPos);
    }

    else if (bRea && (reachPos > (MIN + 2))) {
      reachPos = reachPos - 2;
      servoReach.write(reachPos);
    }

    else if (nRea) { // nuetral states might actually be unnecesary
      reachPos = reachPos;
      servoReach.write(reachPos);
    }

    // Servo 4 - CLAMP: opens/closely completely when triggered (1) = Open, (0) = close
    if (Cla && !nCla && (clampPos < (clampMAX - 2))) {
      for (clampPos = MIN + 2; clampPos <= clampMAX - 2; clampPos += 2) {  // Open
        servoClamp.write(clampPos);
        delay(10);
      }
    }

      else if (!Cla && !nCla && (clampPos > (MIN + 2))) {
        for (clampPos = clampMAX - 2; clampPos >= MIN + 2; clampPos -= 2) {   // Close
          servoClamp.write(clampPos);
          delay(10);
        }
      }

        else if (nCla) {
          clampPos = clampPos;
        }

        //    Serial.print("Clamp Value: ");
        //    Serial.println(clampVal);
        //    Serial.print("Clamp Position: ");
        //    Serial.println(clampPos);



        lastTime = millis();
      }
    }
