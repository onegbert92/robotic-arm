/**************************
Server Code
Accelerometers for Reach, OMG, Yaw, Clamp
**************************/

#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>
#include <Wire.h>

//BLE server name
#define bleServerName "OT_ESP32"

// Constants for accelerometer//EMG
const int REACHPIN = 34;              // Reach-axis
const int OMGPIN = 38;                // OMG-axis
const int YAWPIN = 37;                // Yaw-axis
const int EMGPIN = 2;                 // Has to be an Analog Pin

// Constants for converting accelerometer signal
const int ADC_num_bits = 12;
const int ADC_int = 4095;             // 2^12 bits
const int GND = 0;
float sensorG = 3.3;

// Constants for converting EMG signal
int sensorValue;
int VOLTAGE = 3.3;
float voltage;

// Variables used to express signal in different units
int rawReach, rawOMG, rawYaw, rawClamp;
float gR, gO, gY, gC;

float mapf(float x, float in_min, float in_max, float out_min, float out_max) {
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

// Timer variables
unsigned long lastTime = 0;
unsigned long timerDelay = 35; // allows sufficient time for communication 

bool deviceConnected = false;

// Service UUID
#define SERVICE_UUID "eb8f7231-c8f4-4b54-a38e-99507c501c0e"

// Reach-Axis Characteristic & Descriptor
BLECharacteristic reachCharacteristics("4bc66632-6c6a-41d5-adf6-5dc4e2b04c80", BLECharacteristic::PROPERTY_NOTIFY);
BLEDescriptor reachDescriptor(BLEUUID((uint16_t)0x2902));

// OMG-Axis Characteristic & Descriptor
BLECharacteristic omgCharacteristics("92d4c053-f0e6-4d73-ad7a-76c486de19ba", BLECharacteristic::PROPERTY_NOTIFY);
BLEDescriptor omgDescriptor(BLEUUID((uint16_t)0x2903));

// Yaw-Axis Characteristic & Descriptor
BLECharacteristic yawCharacteristics("2f4d7732-2020-4e93-8d56-ea90122ecff0", BLECharacteristic::PROPERTY_NOTIFY);
BLEDescriptor yawDescriptor(BLEUUID((uint16_t)0x2901));

// Clamp-Axis Characteristic & Descriptor
BLECharacteristic clampCharacteristics("f3c9d0f3-dbcb-4219-870c-e776fd34d371", BLECharacteristic::PROPERTY_NOTIFY);
BLEDescriptor clampDescriptor(BLEUUID((uint16_t)0x2904));

//Setup callbacks onConnect and onDisconnect
class MyServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
      deviceConnected = true;
    };
    void onDisconnect(BLEServer* pServer) {
      deviceConnected = false;
    }
};


void setup() {

// Create the BLE Device
  BLEDevice::init(bleServerName);

  // Create the BLE Server
  BLEServer *pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());

  // Create the BLE Service
  BLEService *botService = pServer->createService(SERVICE_UUID);

  // Create BLE Characteristics and Create a BLE Descriptor
  // Reach-axis
  botService->addCharacteristic(&reachCharacteristics);
  reachDescriptor.setValue("Reach Value");
  reachCharacteristics.addDescriptor(new BLE2902());

  // OMG-axis
  botService->addCharacteristic(&omgCharacteristics);
  omgDescriptor.setValue("OMG Value");
  omgCharacteristics.addDescriptor(new BLE2902());

  // Yaw-Axis
  botService->addCharacteristic(&yawCharacteristics);
  yawDescriptor.setValue("Yaw Value");
  yawCharacteristics.addDescriptor(new BLE2902());

  // Clamp-Axis
  botService->addCharacteristic(&clampCharacteristics);
  clampDescriptor.setValue("Clamp Value");
  clampCharacteristics.addDescriptor(new BLE2902());

  // Start the service
  botService->start();

  // Start advertising
  BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
  pAdvertising->addServiceUUID(SERVICE_UUID);
  pServer->getAdvertising()->start();
  
Serial.begin(9600);

}

void loop() {

   if (deviceConnected) {
    if ((millis() - lastTime) > timerDelay) {

      // Get Raw Sensor Data
      rawReach = analogRead(REACHPIN);
      delay(1);
      rawOMG = analogRead(OMGPIN);
      delay(1);
      rawYaw = analogRead(YAWPIN);
      delay(1);
      sensorValue = analogRead(EMGPIN);
      delay(1);

      // Map raw value to G-force or Voltage 
      gR = mapf(rawReach, 0, ADC_int, -sensorG, sensorG);
      gO = mapf(rawOMG, 0, ADC_int, -sensorG, sensorG);
      gY = mapf(rawYaw, 0, ADC_int, -sensorG, sensorG);
      gC = sensorValue * (VOLTAGE / 1023.0); // voltage for EMG
      Serial.println(gC);

      //Notify Reach-Value from accelerometer
      static char rTemp[6];                
      dtostrf(gR, 6, 2, rTemp);
      //Set Reach-axis Characteristic value and notify connected client
      reachCharacteristics.setValue(rTemp);
      reachCharacteristics.notify();
      delay(1);

      //Notify OMG-Value from accelerometer
      static char oTemp[6];
      dtostrf(gO, 6, 2, oTemp);
      //Serial.print("omg char: "); Serial.println(oTemp);
      //Set OMG-axis Characteristic value and notify connected client
      omgCharacteristics.setValue(oTemp);
      omgCharacteristics.notify();
      delay(1);

      //Notify Yaw-Value from accelerometer
      static char yTemp[6];
      dtostrf(gY, 6, 2, yTemp);
      //Set Yaw Characteristic value and notify connected client
      yawCharacteristics.setValue(yTemp);
      yawCharacteristics.notify();
      delay(1);

      //Notify Clamp-Value from EMG
      static char cTemp[6];
      dtostrf(gC, 6, 2, cTemp);
      //Set Clamp Characteristic value and notify connected client
      clampCharacteristics.setValue(cTemp);
      clampCharacteristics.notify();
      delay(1);
 
      lastTime = millis();
    }
  }

}
